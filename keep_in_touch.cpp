/**
 * This code runs on each of the 2 Freescale FRDM mbed
 * boards used in the project.
 * Niranjan Kumar, Amy Liao, Eric Tu, Siddarth Sen
 */

#include "mbed.h"

// The green LED.
DigitalOut myled(LED2);

// The serial connection to the PC.
Serial pc(USBTX, USBRX);

// Keeps time.
Ticker timer;
int t;

// 0 if this is the left mbed.
// 1 if this is the right mbed.
int side;

// 1 if the mbed is sending data to the PC.
// 0 otherwise.
int send;

// Thresholds
int soft_press = 100000; // 100 kOhms
int hard_press = 10000; // 10 kOhms

int press_counter = 0;
int shift = 0;
int output = -1;
float resistances[5] = {0.0,0.0,0.0,0.0,0.0};
float vread = 0.0;

AnalogIn V1(PTB0);
AnalogIn V2(PTB1);
AnalogIn V3(PTB2);
AnalogIn V4(PTB3);
AnalogIn V5(PTC2);


void computeResistances() {
    vread = V1.read();
    resistances[0] = (1000.0*(1.0-vread))/vread;

    vread = V2.read();
    resistances[1] = (1000.0*(1.0-vread))/vread;

    vread = V3.read();
    resistances[2] = (1000.0*(1.0-vread))/vread;

    vread = V4.read();
    resistances[3] = (1000.0*(1.0-vread))/vread;

    vread = V5.read();
    resistances[4] = (1000.0*(1.0-vread))/vread;

}



/**
 * Sends the finger ID and timestamp over serial connection
 * to the PC.
 * The finger ID is an integer 0-9.
 * 0 = left small "pinky" finger
 * 1 = left ring finger
 * 2 = left middle finger
 * 3 = left index finger
 * 4 = left thumb
 * 5 = right thumb
 * 6 = right index finger
 * 7 = right middle finger
 * 8 = right ring finger
 * 9 = right small "pinky" finger
 * If the finger is pressed to hard, add 10 to the finger ID.
 * A finger ID of 20 indicates that either multiple fingers
 * are being pressed or that the finger is unknown.
 * Therefore, the finger ID sent is in the range 0-20.
 * This can fit in a signed 8-bit integer (a char).
 *
 * However, a single mbed is not capable of sensing all the fingers.
 * The left mbed will sense fingers 0-4 and right will sense 5-9.
 * If side == 0, this is the left mbed. If side == 1, this is the right mbed.
 *
 * The variable time holds the time in milliseconds since the mbed
 * started. Just send this value as the timestamp. (already done for you)
 * (The Java program will correct for the offset from PC system time.)
 * This will probably take 3 bytes.
 *
 * Therefore, a total of 4 bytes will be sent over
 * serial connection every time this function is called.
 */
void sendFingerData() {
    // TODO (Sid)
    // YOUR CODE HERE

    computeResistances();

    if (side == 1){
        shift =5;
    }

    for(int i=0; i<5; i++){
        //pc.printf("Hello: %f\n", resistances[i]);
        if (resistances[i] < hard_press) {
            output = shift + i + 10;
            press_counter++;
        }
        else if (resistances[i] < soft_press) {
            output = shift + i;
            press_counter++;
        }
        else {
            continue;
        }
    }

    if (press_counter == 1){
        //pc.printf("%d ", output);
        pc.putc(output);
    }
    else {
        //pc.printf("%d ", 20);
        pc.putc(20);
    }

    shift = 0;
    press_counter = 0;
    
    // Sends the time to the pc in 3 bytes (little endian format)
    //pc.printf("Time: %d ", t);
    pc.putc(t);
    pc.putc(t >> 8);
    pc.putc(t >> 16);
    t += 25;

    // Every 2 seconds, indicates to the PC that it is time to read data from
    // the serial port buffer. This is to prevent overflow of the buffer.
    if (t % 2000 == 1975) {
        //pc.printf("%d ", -1);
        pc.putc(-1);
    }
}



/**
 * Responsible to stopping the streaming of data to the pc.
 */
void stopSending() {
    // Indicates that the PC should read the data from
    // the serial port buffer. This makes sense since no
    // more data will be sent to the PC.
    pc.putc(-1);

    // No longer in sending mode.
    send = 0;
}

/**
 * The main entry point into the program.
 * Once a signal from the PC is received, the mbed
 * sends finger data 25 times per second (every 40 ms).
 */
int main() {
    // Blocks until the PC sends a 1 byte signal.
    // The pc sends a 0 if this is the left mbed
    // or a 1 if this is the right mbed.
    side = pc.getc();

    // From now on, send finger data 40 times per second.
    // 40 Hz sampling rate implies a sampling period of 25 ms.
    t = 0;
    timer.attach(&sendFingerData, 0.025F);

    // An interrupt generated when the PC next talks to the mbed
    // through the serial port will turn off the streaming of data.
    send = 1;
    pc.attach(&stopSending);
    while (send);
}
