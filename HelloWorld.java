import org.mbed.RPC.*;
public class HelloWorld implements mbedRPC{
	DigitalOut d1;
	DigitalOut d2;
	mbed mbed;
	
	public static void main(String[] args) {
		HelloWorld demo = new HelloWorld();
		demo.flash(10);
	}
	
	public HelloWorld(){	
		System.out.println("mbed RPC Hello World");
		//Create an mbed object for communication over serial
		mbed = new SerialRxTxRPC("COM5", 9600);
		//Or:   mbed = new HTTPRPC("http://192.168.2.2")
		
		//Create new Digital Outputs on the mbed
		d1 = new DigitalOut(mbed, "myled1");
		d2 = new DigitalOut(mbed, "myled2");
	}
	public void flash(int n){
		
		for(int i = 0; i< n; i++){
                    //Call the DigitalOut objects' methods 
			d1.write(1);
			d2.write(0);
			wait(500);
			d1.write(0);
			d2.write(1);
			wait(500);
		}
		mbed.delete();
		System.out.println("Complete");
		
	}
	//A function to create a wait
	public static void wait (int n){
        long startTime,currentTime;
        startTime =System.currentTimeMillis();
        do{
            currentTime = System.currentTimeMillis();
        }
        while ((currentTime - startTime) < n);
	}
}
