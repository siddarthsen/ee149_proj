/**
 * This class is a reduced version of TypingFramework designed for testing purposes.
 * It only needs one mbed to work.
 * Niranjan Kumar, Eric Tu, Amy Liao, Siddarth Sen
 *
 * Sources:
 * Jarvi, Trent. et al. "RXTX: A Java Cross-Platform Wrapper Library
 * for the Serial Port" 1997-2014 The GNU Project.
 * https://github.com/rxtx
 * Made available under the GNU Lesser General Public License (LGPL)
 * http://www.gnu.org/licenses/lgpl.txt
 */

import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

// RXTX Library imports
import gnu.io.CommPort;

public class TestingTypingFramework extends TypingFramework {

    /**
     * Creates a new TypingFramework which manages mbedPorts and gui.
     */
    public TestingTypingFramework(CommPort[] mbedPorts, TypingGUI gui) throws IOException {
        // This is just a guess.
        _leftMbedPort = mbedPorts[0];

        // Sets up the input and output streams
        _fromLeftMbed = _leftMbedPort.getInputStream();
        _toLeftMbed = _leftMbedPort.getOutputStream();

		_gui = gui;

        _mbedFingers = new ArrayList<Integer>();
        _mbedFingerTimes = new ArrayList<Long>();
        _mbedTooHard = new ArrayList<Boolean>();
    }

    /**
     * Synchronizes clocks with the mbeds and instructs each to start
     * sending a stream of data via the serial port.
     */
    public void synchronizeWithMbed() throws IOException {
        _toLeftMbed.write(0);
        _startTime = System.currentTimeMillis();
    }

    /**
     * Tells the mbeds that it is time to stop collecting data.
     */
    protected void stopCollectingMbedData() throws IOException {
        _toLeftMbed.write(-1);
    }

    /**
     * Closes the streams of communication to and from the mbeds.
     */
    protected void endMbedCommunication() throws IOException {
        _toLeftMbed.close();
        _fromLeftMbed.close();
    }

    /**
     * Reads data in from the mbeds.
     */
    protected void recordMbedData() throws IOException {
        int leftFinger = _fromLeftMbed.read();
        int leftMbedTime = -1;
        while (leftFinger != 255) {
            leftMbedTime = _fromLeftMbed.read();
            leftMbedTime = leftMbedTime | (_fromLeftMbed.read() << 8);
            leftMbedTime = leftMbedTime | (_fromLeftMbed.read() << 16);
            leftMbedTime = leftMbedTime | (_fromLeftMbed.read() << 24);
            storeMbedData(leftFinger, leftMbedTime);
            leftFinger = _fromLeftMbed.read();
        }
    }


    /**
     * Uses the data from the mbed to calculate which fingers were used.
     * Dummy implementation for testing purposes.
     * It currently assumes all fingers are used correctly.
     * Edit this to see how the code handles user mistakes.
     */
    public void calculateFingersUsed() {
        int maxAllowableError = 40;
        int lower = 0;
        int upper = _mbedFingers.size() - 1;
        int index = 0;
        for(int i=0; i<_charsTyped.length; i++){
            char key = _charsTyped[i];
            long keyTime = _timesCharsTyped[i];
            if ((charToFinger(key) == 0) || (charToFinger(key) == 1) || (charToFinger(key) == 2) || (charToFinger(key) == 3) || (charToFinger(key) == 4))  {
                while(true){
                index = (upper+lower)/2;
                if(Math.abs(keyTime - _mbedFingerTimes.get(index)) < maxAllowableError){
                    _fingersUsed[i] = _mbedFingers.get(index);
                    break;
                }
                if(upper == lower){
                    _fingersUsed[i] = 20; // failed to identify finger
                    break;
                }
                if (upper - lower == 1) {
                    lower = upper;
                    continue;
                }
                if((keyTime - _mbedFingerTimes.get(index)) > 0){
                    lower = index;
                }
                else{
                    upper = index;
                }
            }
            lower = 0;
            upper = _mbedFingers.size() - 1;
            }
            else{
                _fingersUsed[i] = charToFinger(key);
            }


        }
    }

}
