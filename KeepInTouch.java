/**
 * EECS 149 Project: "Keep In Touch"
 * This is the typing program which runs on the host PC.
 * It collects data regarding finger usage and pressure from the
 * Freescale mbed FRDM KL25Z, which is connected via USB.
 * Niranjan Kumar, Eric Tu, Amy Liao, Siddarth Sen
 *
 * Sources:
 * Jarvi, Trent. et al. "RXTX: A Java Cross-Platform Wrapper Library
 * for the Serial Port" 1997-2014 The GNU Project.
 * https://github.com/rxtx
 * Made available under the GNU Lesser General Public License (LGPL)
 * http://www.gnu.org/licenses/lgpl.txt
 */

import java.awt.event.KeyListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Enumeration;

// RXTX Library imports
import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;

public class KeepInTouch {

    /**
     * The name of the file the passage is stored in.
     */
    public static final String PASSAGE = "passage.txt";

    /**
     * Entry point into the program.
     * The serial port connection is set up here.
	 * Relevant objects are initialized here.
     * Exception handling is done here.
     */
    public static void main(String[] args) {
        TypingFramework tf = null;
		TypingGUI gui;
        TypingKeyListener tkl;
        CommPortIdentifier portID;
        CommPort[] mbedPorts = new CommPort[2];
        int portNum = 0;
		
		// Tries to find the mbed port.
		// Prints an error message and exits upon failure.
		Enumeration<CommPortIdentifier> portIDs = CommPortIdentifier.getPortIdentifiers();
        while (portNum < 2 && portIDs.hasMoreElements()) {
            portID = (CommPortIdentifier) portIDs.nextElement();
	        if (portID.getPortType() == CommPortIdentifier.PORT_SERIAL) {
				try {
				    mbedPorts[portNum] = portID.open("Keep In Touch", 5000);
					System.out.printf("The number %d mbed port is %s\n", portNum, mbedPorts[portNum].getName());
					portNum++;
				} catch (PortInUseException piue) {
                    // do nothing
				}
		    }
        }
		if (portNum == 1 && args.length == 0) {
            System.err.println("Unable to connect with one of the Freescale mbed FRDM KL25Z boards.");
            System.err.println("Check if both are plugged in.");
            System.err.println("If they are plugged in, another program might be busy using it.");
			System.err.println("Close that program (or wait for it to finish) and try again.");
            System.exit(0);
        } else if (portNum == 0) {
            System.err.println("Unable to connect with either of the Freescale mbed FRDM KL25Z boards.");
            System.err.println("Check if they are plugged in.");
            System.err.println("If they are plugged in, another program might be busy using them.");
			System.err.println("Close that program (or wait for it to finish) and try again.");
            System.exit(0);
        }

		// Tries to create all the necessary objects.
		// Prints an error message and terminates upon failure.
		gui = new TypingGUI();
        try {
            if (args.length == 0) {
                tf = new TypingFramework(mbedPorts, gui);
            } else {
                tf = new TestingTypingFramework(mbedPorts, gui);
            }
        } catch (IOException ioe) {
            System.err.println("Communications with Freescale mbed FRDM KL25Z interrupted.");
            System.err.println("See error message below for details:");
            ioe.printStackTrace();
            System.exit(0);
        }
		tkl = new TypingKeyListener(tf);
		gui.addKeyListener(tkl);

		// Executes the core of the typing learning software.
        try {
            tf.loadPassage(PASSAGE);
        } catch (FileNotFoundException fnfe) {
            System.err.println("The passage file " + PASSAGE + " was not found.");
            System.err.println("Please check if you are running the program from the correct directory.");
            System.exit(0);
        } catch (IOException ioe) {
            System.err.println("Error reading from " + PASSAGE);
            System.exit(0);
        }

        try {
            tf.synchronizeWithMbed();
            tf.record();
        } catch (IOException ioe) {
            System.err.println("Communications with Freescale mbed FRDM KL25Z interrupted.");
            System.err.println("See error message below for details:");
            ioe.printStackTrace();
            System.exit(0);
        }
        tf.calculateFingersUsed();
        tf.displayFeedback();
        tf.displayStatistics();
    }
}
