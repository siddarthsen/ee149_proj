/**
 * This class represents a framework for a typing program.
 * It presents a typing assignment to the user.
 * It senses which keys are pressed. It also senses which finger is used by
 * interfacing with the mbed over USB, with help of the RXTX library.
 * With this data, it provides feedback to the user.
 * Niranjan Kumar, Eric Tu, Amy Liao, Siddarth Sen
 *
 * Sources:
 * Jarvi, Trent. et al. "RXTX: A Java Cross-Platform Wrapper Library
 * for the Serial Port" 1997-2014 The GNU Project.
 * https://github.com/rxtx
 * Made available under the GNU Lesser General Public License (LGPL)
 * http://www.gnu.org/licenses/lgpl.txt
 */

import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

// RXTX Library imports
import gnu.io.CommPort;

public class TypingFramework {

    /**
     * Creates a new TypingFramework which manages mbedPorts and gui.
     */
    public TypingFramework(CommPort[] mbedPorts, TypingGUI gui) throws IOException {
        // This is just a guess.
        _leftMbedPort = mbedPorts[0];
        _rightMbedPort = mbedPorts[1];

        // Sets up the input and output streams
        _fromLeftMbed = _leftMbedPort.getInputStream();
        _toLeftMbed = _leftMbedPort.getOutputStream();
        _fromRightMbed = _rightMbedPort.getInputStream();
        _toRightMbed = _rightMbedPort.getOutputStream();

		_gui = gui;

        _mbedFingers = new ArrayList<Integer>();
        _mbedFingerTimes = new ArrayList<Long>();
        _mbedTooHard = new ArrayList<Boolean>();
    }

    /**
     * For testing purposes.
     * See OneHandTypingFramework for details.
     */
    protected TypingFramework() throws IOException {
    }

    /**
     * Loads the passage stored in the file passageFile.
     */
    public void loadPassage(String passageFileName) throws IOException {
        File passageFile = new File(passageFileName);

        _passageLength = (int) passageFile.length();
        _passage = new char[_passageLength];
        _charsTyped = new char[_passageLength];
        _timesCharsTyped = new long[_passageLength];
        _fingersUsed = new int[_passageLength];
        FileReader passageReader = new FileReader(passageFile);
        passageReader.read(_passage);
        _currIndex = 0;

        displayPassage();
    }

    /**
     * Displays the passage.
     */
    private void displayPassage() {
        // This is purely GUI work.
        // The contents of the passage must be displayed on the GUI frame.
        // Care must be taken to wrap the lines properly.

        // Note:
        // All actual drawing should be done inside the paint() method
        // of the TypingGUI class, so this will end up calling methods
        // in that class. However, you should never call paint() directly.
        // The method inside the TypingGUI class should call repaint()
        // to update the image. Never call repaint() inside the body of paint().
        // System.out.println(_passage);
        String displayed = new String(_passage);
        _gui.setSentence(displayed, _passage);
    }

    /**
     * Synchronizes clocks with the mbeds and instructs each to start
     * sending a stream of data via the serial port.
     */
    public void synchronizeWithMbed() throws IOException {
        _toLeftMbed.write(0);
        _toRightMbed.write(1);
        _startTime = System.currentTimeMillis();
    }

    /**
     * Records and stores the user input.
     */
    public void record() throws IOException {
        // System.out.println("Called record()"); /////////////DEBUG
        boolean first = true;
        long last = _startTime;
        long now;
        // Mbed data will be recorded every 2 seconds, but the very
        // first time, we will wait for 3 seconds, hence the 3000.
        // This is because InputStream.read() blocks waiting for input
        // and that behavior should be avoided. A 255 (-1 on mbed) token
        // arrives every 2 seconds, so we wait 3 seconds to make sure
        // we get the first one.
        long interval = 3000;
        while (_currIndex < _passageLength) {
            // System.out.println(_currIndex);
            indicateNextChar();
            _keyHasBeenTyped = false;
            // Wait for keyboard input.
            while (!_keyHasBeenTyped) {
                // Records data from the mbeds every 2 seconds so that the
                // input streams carrying data from the serial ports do not
                // overflow. The very first time, we wait for 3 seconds.
                now = System.currentTimeMillis();
                if (now - last > interval) {
                    recordMbedData();
                    last = now;
                    if (interval == 3000) {
                        interval = 2000;
                    }
                }
            }
            _currIndex++;
        }
        _endTime = System.currentTimeMillis();
        stopCollectingMbedData();
        recordMbedData();
        endMbedCommunication();
    }

    /**
     * Tells the mbeds that it is time to stop collecting data.
     */
    protected void stopCollectingMbedData() throws IOException {
        _toLeftMbed.write(-1);
        _toRightMbed.write(-1);
    }

    /**
     * Closes the streams of communication to and from the mbeds.
     */
    protected void endMbedCommunication() throws IOException {
        _toLeftMbed.close();
        _fromLeftMbed.close();
        _toRightMbed.close();
        _toRightMbed.close();
    }

    /**
     * Reads data in from the mbeds.
     */
    protected void recordMbedData() throws IOException {
        int leftFinger = 256;
        int leftMbedTime = -1;
        int rightFinger = _fromRightMbed.read();
        int rightMbedTime = _fromRightMbed.read();
        rightMbedTime = rightMbedTime | (_fromRightMbed.read() << 8);
        rightMbedTime = rightMbedTime | (_fromRightMbed.read() << 16);
        boolean readingLeft = true;
        while (leftFinger != 255 || rightFinger != 255) {
            if (readingLeft) {
                leftFinger = _fromLeftMbed.read();
                System.out.printf("Read finger %d from left mbed.\r\n", leftFinger); //////////////////////DEBUG
                if (leftFinger != 255) {
                    leftMbedTime = _fromLeftMbed.read();
                    leftMbedTime = leftMbedTime | (_fromLeftMbed.read() << 8);
                    leftMbedTime = leftMbedTime | (_fromLeftMbed.read() << 16);
                    if (rightFinger == 255 || leftMbedTime < rightMbedTime) {
                        storeMbedData(leftFinger, leftMbedTime);
                    } else {
                        storeMbedData(rightFinger, rightMbedTime);
                        readingLeft = false;
                    }
                } else {
                    readingLeft = false;
                }
            } else {
                rightFinger = _fromRightMbed.read();
                if (rightFinger != 255) {
                    rightMbedTime = _fromRightMbed.read();
                    rightMbedTime = rightMbedTime | (_fromRightMbed.read() << 8);
                    rightMbedTime = rightMbedTime | (_fromRightMbed.read() << 16);
                    if (leftFinger == 255 || rightMbedTime <= leftMbedTime) {
                        storeMbedData(rightFinger, rightMbedTime);
                    } else {
                        storeMbedData(leftFinger, leftMbedTime);
                        readingLeft = true;
                    }
                } else {
                    readingLeft = true;
                }
            }
        }
    }

    /**
     * Stores the mbed finger, time, and too hard data.
     */
    protected void storeMbedData(int finger, int mbedTime) {

        // Only store the data if the finger is known.
        // This means finger code 20 (unknown/multiple) fingers
        // will be ignored.
        if (finger < 20) { // SHOULD BE UNCOMMENTED FOR PROPER FUNCTION
            if (finger < 10) {
                _mbedFingers.add(finger);
                _mbedTooHard.add(false);
            } else {
                _mbedFingers.add(finger - 10);
                _mbedTooHard.add(true);
            }

            // Convert from mbed time to PC time.
            _mbedFingerTimes.add(_startTime + mbedTime);
        } // SHOULD BE UNCOMMENTED FOR PROPER FUNCTION
    }

    /**
     * Indicates the next character the user must type.
     */
    protected void indicateNextChar() {
        // TODO (Eric)
        // This is GUI work.
        // This method should indicate which character the user has
        // to type next. This can be done by highlighting the character,
        // underlining it, or drawing a ^ or arrow underneath.
        // The private instance field _currIndex will come in handy.
        // Also note that the underlining or pointing or highlighting
        // of the previous character needs to be erased.
        // For example:
        // The quick brown fox jumps over the lazy dog.
        //             ^^
        // is bad because there the previous ^ was not erased.

        // Note:
        // All actual drawing should be done inside the paint() method
        // of the TypingGUI class, so this will end up calling methods
        // in that class. However, you should never call paint() directly.
        // The method inside the TypingGUI class should call repaint()
        // to update the image. Never call repaint() inside the body of paint().
        String indicator = "^";
        String space = " ";
        for (int i =0; i<_currIndex; i++) {
            indicator = space + indicator;
        }
        _gui.setIndicator(indicator, _currIndex);

    }

    /**
     * Uses the data from the mbed to calculate which fingers were used.
     */
    public void calculateFingersUsed() {
        // TODO (Sid)
        int maxAllowableError = 40;
        int lower = 0;
        int upper = _mbedFingers.size() - 1;
        int index = 0;
        for(int i=0; i<_charsTyped.length; i++){
            char key = _charsTyped[i];
            long keyTime = _timesCharsTyped[i];
            while(true){
                index = (upper+lower)/2;
                if(Math.abs(keyTime - _mbedFingerTimes.get(index)) < maxAllowableError){
                    _fingersUsed[i] = _mbedFingers.get(index);
                    break;
                }
                if(upper == lower){
                    _fingersUsed[i] = 20; // failed to identify finger
                    break;
                }
                if (upper - lower == 1) {
                    lower = upper;
                    continue;
                }
                if((keyTime - _mbedFingerTimes.get(index)) > 0){
                    lower = index;
                }
                else{
                    upper = index;
                }
            }
            lower = 0;
            upper = _mbedFingers.size() - 1;
        }

    }

    /**
     * Displays feedback to the user.
     * This includes things like wrong key, wrong finger,
     * and pressed key too hard.
     */
    public void displayFeedback() {
        // TODO (Eric)
        // A combination of calculation and GUI work.
        // A simple way to do this is to write what the
        // user actually typed below the original passage,
        // but with the characters colored to indicate the errors.
        // A simple scheme would be blue for wrong character,
        // red for wrong finger, and purple for both.
        // To indicate that the character was pressed too hard,
        // it can be made bold.
        // It will be obvious to the user what character mistake was made
        // by comparing the blue or purple character to the corresponding
        // character in the original passage above.
        // It will also be obvious to the user which characters were keyed
        // in too hard since they will be bold.
        // However, it would not be obvious to the user what the finger
        // error was. Therefore, additional text feedback will be required
        // to tell the user which finger he/she used and what finger it would
        // have been correct to use.

        // Note:
        // All actual drawing should be done inside the paint() method
        // of the TypingGUI class, so this will end up calling methods
        // in that class. However, you should never call paint() directly.
        // The method inside the TypingGUI class should call repaint()
        // to update the image. Never call repaint() inside the body of paint().
        _gui.setFeedback(_passageLength, _charsTyped, _fingersUsed);

    }

    /**
     * Notifies this TypingFramework which key was just typed,
     * and when it was typed.
     * This is called by the TypingKeyListener.
     */
    public void notifyKeyTyped(char key, long time) {
        _charsTyped[_currIndex] = key;
        _timesCharsTyped[_currIndex] = time;
        _keyHasBeenTyped = true;
    }

    /**
     * Generates statistics, such as character accuracy,
     * fingering accuracy, and words per minute (WPM)
     * and displays them.
     */
    public void displayStatistics() {
        // TODO (Eric)
        // This is a combination of calculation and GUI work.

        // Here is some sample code which computes words per minute.
        // It uses the assumption that the average word is 5 characters long.
        double wpm = (_passageLength / 5.0) / ((_endTime - _startTime) / 60000.0);
        int diff = 0;
        for (int i = 0; i<_passageLength; i++) {
            if (_passage[i] != _charsTyped[i]) {
                diff += 1;
            }
        }
        double percentCorrect = (_passageLength-diff)*100.0/(_passageLength*1.0);
        _gui.setStatistics(wpm, percentCorrect);
        // Note:
        // All actual drawing should be done inside the paint() method
        // of the TypingGUI class, so this will end up calling methods
        // in that class. However, you should never call paint() directly.
        // The method inside the TypingGUI class should call repaint()
        // to update the image. Never call repaint() inside the body of paint().
    }

    /**
     * Returns the ID of the finger that should be used to type the
     * character c. Returns -1 if c is invalid.
     * 0 = left small "pinky" finger
     * 1 = left ring finger
     * 2 = left middle finger
     * 3 = left index finger
     * 4 = left thumb
     * 5 = right thumb
     * 6 = right index finger
     * 7 = right middle finger
     * 8 = right ring finger
     * 9 = right small "pinky" finger
     */
    public static int charToFinger(char c) {
        switch (c) {
            case 'q':
            case 'a':
            case 'z':  return 0;
            case 'w':
            case 's':
            case 'x':  return 1;
            case 'e':
            case 'd':
            case 'c':  return 2;
            case 'r':
            case 'f':
            case 'v':
            case 't':
            case 'g':
            case 'b':  return 3;
            case ' ':  return 4;
            case 'y':
            case 'h':
            case 'n':
            case 'u':
            case 'j':
            case 'm':  return 6;
            case 'i':
            case 'k':
            case ',':  return 7;
            case 'o':
            case 'l':
            case '.':  return 8;
            case 'p':
            case ';':
            case '/':  return 9;
        }
        return -1;
    }

    /** The passage that the user is working on. */
    protected char[] _passage;

    /** The length of the passage (number of characters). */
    protected int _passageLength;

    /** The time at which the user started typing. */
    protected long _startTime;

    /** The time at which the user stopped typing. */
    protected long _endTime;

    /** The characters the user has typed on the keyboard. */
    protected char[] _charsTyped;

    /** The times at which the user typed characters on the keyboard. */
    protected long[] _timesCharsTyped;

    /**
     * The fingers used to type each character.
     * This array is filled in the calculateFingersUsed() method.
     */
    protected int[] _fingersUsed;

    /** The index of the character the user is currently working on. */
    protected int _currIndex;

    /** The key which the user has typed. */
    protected char _keyTyped;

    /** The time at which the user has typed the most recent key. */
    protected long _keyTypedTime;

    /** True if the user has typed a key. */
    protected boolean _keyHasBeenTyped;

    /** Stream containing data sent from the left mbed to the PC. */
    protected InputStream _fromLeftMbed;

    /** Stream containing data sent from the PC to the left mbed. */
    protected OutputStream _toLeftMbed;

    /** Stream containing data sent from the right mbed to the PC. */
    private InputStream _fromRightMbed;

    /** Stream containing data sent from the PC to the right mbed. */
    private OutputStream _toRightMbed;

    /**
     * The serial port through which the mbed is connected to the PC.
     * This is an abstraction provided by the RXTX library.
     */
    protected CommPort _leftMbedPort;

    /**
     * The serial port through which the right mbed is connected to the PC.
     * This is an abstraction provided by the RXTX library.
     */
    private CommPort _rightMbedPort;

    /**
     * The list of finger presses extracted from the mbed data.
     */
    protected ArrayList<Integer> _mbedFingers;

    /**
     * The list of times at which the fingers were pressed.
     * The times have been converted to Java system time.
     * Extracted from the mbed data.
     */
    protected ArrayList<Long> _mbedFingerTimes;

    /**
     * This list of booleans records for each of the mbed data
     * points described by _mbedFingers and _mbedFingerTimes
     * whether the key was pressed too hard.
     */
     protected ArrayList<Boolean> _mbedTooHard;

	/** The TypingGUI used to display feedback to the user. */
	protected TypingGUI _gui;

}
