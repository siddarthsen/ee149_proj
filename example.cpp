#include "mbed.h"

// LED1 is red, LED2 is green, and LED3 is blue
DigitalOut myled(LED3);
Serial pc(USBTX, USBRX);

int main() {
    while(1) {
        // Note that 0 is on and 1 is off.
        myled = 1;
        // wait 2.0 seconds
        wait(2.0);
        myled = 0;
        // Transmits "Hello World!" over USB.
        // \r\n is the Windows line terminator.
        // If your PC is not Windows, \n is enough.
        // Note that since this data is sent to the PC,
        // the OS of the PC determines what to do with it.
        // The mbed has no OS.
        pc.printf("Hello world!\r\n");
        // wait 0.5 seconds
        wait(0.5);
    }
}
