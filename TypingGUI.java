/**
 * An instance of this class serves as the GUI for typing learning software.
 * It takes instructions from TypingFramework.
 * Niranjan Kumar, Amy Liao, Eric Tu, Siddarth Sen
 */

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Font;
import javax.swing.JPanel;
import java.awt.geom.Rectangle2D;


public class TypingGUI extends JFrame {
    public TypingGUI() {
	    setVisible(true);
		setSize(900, 600);
		setResizable(false);
		setTitle("Keep in Touch Interactive Learning Program");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// JLabel image = new JLabel();
		// ImageIcon iconLogo = new ImageIcon("logo.png");
		// image.setIcon(iconLogo);
		// add(image);

	}
	public void paint(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		Font f = new Font("monospaced", Font.PLAIN, 18);
		g.setFont(f);
	    g.setColor(Color.WHITE);
		g.fillRect(0, 0, 900, 600);
	    g.setColor(Color.BLACK);
	    g.drawString("Key for What You Typed:", 10, 500);
	    g.drawString("Black: Key Correct, Finger Correct", 10, 515);
	    g.setColor(Color.BLUE);
	    g.drawString("Blue: Key Correct, Finger Wrong", 10, 530);
	    g.setColor(Color.MAGENTA);
	    g.drawString("Magenta: Key Wrong, Finger Correct", 10, 545);
	    g.setColor(Color.RED);
	    g.drawString("Red: Key Wrong, Finger Wrong", 10, 560);
	    g.setColor(Color.BLACK);
		g.drawString(_sentence, 100, 305);
		g.drawString(_indicator, 298, 320);
		g.drawString(_userSentence, 100, 355);
		if (_guiCharsTyped != null) {
			int x = 298;
			for (int i = 0; i<_guiPassageLength; i++) {
				int fingerAtChar = TypingFramework.charToFinger(_guiPassage[i]);
				int fingerAtTyped = TypingFramework.charToFinger(_guiCharsTyped[i]);
				if ((_guiPassage[i] == _guiCharsTyped[i]) && (fingerAtChar == _guiFingersUsed[i])) {
					g.setColor(Color.BLACK);
					String displayed = String.valueOf(_guiCharsTyped[i]);
					g.drawString(displayed, x, 355);
					x +=11;
				}
				else if ((_guiPassage[i] == _guiCharsTyped[i]) && (fingerAtChar != _guiFingersUsed[i])) {
					g.setColor(Color.BLUE);
					String displayed = String.valueOf(_guiCharsTyped[i]);
					g.drawString(displayed, x, 355);
					x +=11;
				}
				else if ((_guiPassage[i] != _guiCharsTyped[i]) && (fingerAtTyped == _guiFingersUsed[i])) {
					g.setColor(Color.MAGENTA);
					String displayed = String.valueOf(_guiCharsTyped[i]);
					g.drawString(displayed, x, 355);
					x +=11;
				} else {
					g.setColor(Color.RED);
					String displayed = String.valueOf(_guiCharsTyped[i]);
					g.drawString(displayed, x, 355);
					x +=11;
				}
			}
		}
		g.setColor(Color.BLACK);
		g.drawString(_percentCorrect, 100, 385);
		g.drawString(_wpm, 100, 370);
		g.setColor(Color.BLUE);
		g2.fill(new Rectangle2D.Double(50, 400, 50, 50));
		g2.fill(new Rectangle2D.Double(125, 400, 50, 50));
		g2.fill(new Rectangle2D.Double(200, 400, 50, 50));
		g2.fill(new Rectangle2D.Double(275, 400, 50, 50));
		g2.fill(new Rectangle2D.Double(350, 400, 50, 50));

		g2.fill(new Rectangle2D.Double(450, 400, 50, 50));
		g2.fill(new Rectangle2D.Double(525, 400, 50, 50));
		g2.fill(new Rectangle2D.Double(600, 400, 50, 50));
		g2.fill(new Rectangle2D.Double(675, 400, 50, 50));
		g2.fill(new Rectangle2D.Double(750, 400, 50, 50));

		if (_fingerAtChar == 0) {
			g.setColor(Color.YELLOW);
			g2.fill(new Rectangle2D.Double(50, 400, 50, 50));
		} else if (_fingerAtChar == 1) {
			g.setColor(Color.YELLOW);
			g2.fill(new Rectangle2D.Double(125, 400, 50, 50));
		} else if (_fingerAtChar == 2) {
			g.setColor(Color.YELLOW);
			g2.fill(new Rectangle2D.Double(200, 400, 50, 50));
		} else if (_fingerAtChar == 3) {
			g.setColor(Color.YELLOW);
			g2.fill(new Rectangle2D.Double(275, 400, 50, 50));
		} else if (_fingerAtChar == 4) {
			g.setColor(Color.YELLOW);
			g2.fill(new Rectangle2D.Double(350, 400, 50, 50));
			g2.fill(new Rectangle2D.Double(450, 400, 50, 50));
		} else if (_fingerAtChar == 5) {
			g.setColor(Color.YELLOW);
			g2.fill(new Rectangle2D.Double(450, 400, 50, 50));
			g2.fill(new Rectangle2D.Double(350, 400, 50, 50));
		} else if (_fingerAtChar == 6) {
			g.setColor(Color.YELLOW);
			g2.fill(new Rectangle2D.Double(525, 400, 50, 50));
		} else if (_fingerAtChar == 7) {
			g.setColor(Color.YELLOW);
			g2.fill(new Rectangle2D.Double(600, 400, 50, 50));
		} else if (_fingerAtChar == 8) {
			g.setColor(Color.YELLOW);
			g2.fill(new Rectangle2D.Double(675, 400, 50, 50));
		} else if (_fingerAtChar == 9) {
			g.setColor(Color.YELLOW);
			g2.fill(new Rectangle2D.Double(750, 400, 50, 50));
		}
		Font f2 = new Font("Garamond", Font.PLAIN, 40);
		g.setFont(f2);
		g.setColor(Color.BLACK);
		g.drawString("Keep in Touch", 325, 200);
		
	}
	public void setSentence(String sentenceToDraw, char[] passage) {
		String showSentence = "Sentence to Type: " + sentenceToDraw;
		_guiPassage = passage;
	    _sentence = showSentence;
		repaint();
	}
	public void setIndicator(String indicator, int index) {
		_indicator = indicator;
		_index = index;
		_fingerAtChar = TypingFramework.charToFinger(_guiPassage[_index]);
		repaint();
	}
	public void setFeedback(int passageLength, char[] charsTyped, int[] fingersUsed) {
		_guiCharsTyped = charsTyped;
		_guiPassageLength = passageLength;
		_guiFingersUsed = fingersUsed;
		repaint();
	}
	public void setStatistics(double wpm, double percentCorrect) {
		String newWpm = String.valueOf(wpm);
		String showWPM = "Words Per Minute: " + newWpm;
		String newPercentCorrect = String.valueOf(percentCorrect);
		String showPercentCorrect = "Percent Correct: " + newPercentCorrect;
		_percentCorrect = showPercentCorrect;
		_wpm = showWPM;
		repaint();
	}
	private String _sentence = "Sentence to Type: ";
	private String _indicator = "^";
	private String _userSentence = "You Typed This: ";
	private String _wpm = "Words Per Minute: ";
	private String _percentCorrect = "Percent Correct: ";
	private char[] _guiPassage;
	private char[] _guiCharsTyped;
	private int _guiPassageLength = 0;
	private int[] _guiFingersUsed;
	private int _index = 0;
	private int _fingerAtChar = 0;
}
