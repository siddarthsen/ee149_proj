/**
 * This program just outputs to standard output whatever data it receives
 * over the serial port connection.
 */

import gnu.io.*;
import java.io.InputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;

public class Example {

  // Replace "COM3" with the name of the mbed serial port on your PC.
  public static final String MBED_PORT_NAME = "COM3";

  public static void main(String[] args) {
    CommPort mbedPort = null;
    Enumeration<CommPortIdentifier> portIDs = CommPortIdentifier.getPortIdentifiers();
    System.out.println("These are all the serial ports available:");
    while (portIDs.hasMoreElements()) {
      CommPortIdentifier portID = portIDs.nextElement();
      if (portID.getPortType() == CommPortIdentifier.PORT_SERIAL) {
        System.out.println(portID.getName());
        if (portID.getName().equals(MBED_PORT_NAME)) {
          // 5000 indicates that the program will wait 5000 ms (5 s) to get
          // access to the port. After this time expires, there will be
          // a PortInUseException
          try {
            mbedPort = portID.open("foo", 5000);
          } catch (PortInUseException piue) {
            System.out.println("Unable to get access to mbed serial port within 5 sec.");
          }
        }
      }
    }
    System.out.println();
    if (mbedPort != null) {
      System.out.println("Printing out first 256 bytes of data received from mbed:");
      System.out.println("Each byte is converted to a character before printing.");
      try {
        InputStream mbedInStream = mbedPort.getInputStream();
        int data = 0;
        int counter = 0;
        while (counter < 300) {
          data = mbedInStream.read();
          if (data != -1) {
            counter++;
            System.out.print((char) data);
          }
        }
        mbedInStream.close();
        mbedPort.close();
        System.out.println();
        System.out.println("Finished reading 300 characters.");
      } catch (IOException ioe) {
        System.out.println("Communication disrupted.");
      }
    }
  }
}