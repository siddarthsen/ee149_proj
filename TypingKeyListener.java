/**
 * An instance of this class communicates with an instance of TypingFramework
 * and notifies it when a key has been pressed.
 * Niranjan Kumar, Eric Tu, Amy Liao, Siddarth Sen
 */

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class TypingKeyListener implements KeyListener {

    /**
     * Creates a new TypingKeyListener object which communicates
     * with the TypingFramework tf.
     */
    public TypingKeyListener(TypingFramework tf) {
        _tf = tf;
    }

    /**
     * This method is inteded to remain blank. Including this method
     * is a requirement of implementing the KeyListener interface.
     */
    public void keyPressed(KeyEvent event) {
    }

    /**
     * This method is inteded to remain blank. Including this method
     * is a requirement of implementing the KeyListener interface.
     */
    public void keyReleased(KeyEvent event) {
    }

    /**
     * This method is triggered when a key is typed.
     * It notifies the TypingFramework.
     */
    public void keyTyped(KeyEvent event) {
         long time = System.currentTimeMillis();
        _tf.notifyKeyTyped(event.getKeyChar(), time);
    }

    /** The TypingFramework this TypingKeyListener communicates with. */
    private TypingFramework _tf;

}
